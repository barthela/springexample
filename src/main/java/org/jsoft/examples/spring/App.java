package org.jsoft.examples.spring;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

        Person p = (Person) context.getBean("Person");
        System.out.println("FirstName: " + p.getFirstName() + ", LastName: " + p.getLastName() + ", Car: " + p.getCar().getManufacturer() + " " + p.getCar().getModel());

    }
}
