package org.jsoft.examples.spring;

/**
 * Created by andreas on 6/9/14.
 */
public class Person {

    private String firstName;
    private String lastName;
    private Car car;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
